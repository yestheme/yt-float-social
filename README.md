YT Float Social - Joomla! Social Module
=========

YT Float Social is simple and one of the best social module for Joomla! This module developed by YesTheme and using some social network API to display the social data of your data you have shared to social networking sites. This module is very useful for your website, it is easy to install, config and use!

Compatibility:
-
- Joomla! 3.0
- Joomla! 2.5

Features:
-
- Display Facebook Like box, Recommendations Box, Recommendations Bar (And more in future releases)
- Display Twitter timeline widgets
- Display Pinterest Pin Widgets, Profile Widgets, Board Widgets
- Display the module at the certain positions or as floating icons

Version
-
1.0.0

Changelog
-
-------------------- [YT Float Social Version 1.0.0] --------------------

Initial release of YT Float Social

Install and use
-
> Download the module, install to your Joomla! Site and publish it to any position you want to show it!

License
-
GNU/GPL