<?php
/*
 * assets.php - mod_yt_float_social - YT Float Social by YesTheme
 * ------------------------------------------------------------------------
 *   Author     YesTheme http://yestheme.com
 *   Copyright (C) 2008 - 2013 YesTheme.com. All Rights Reserved.
 *   @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 *   Websites   http://yestheme.com
 * ------------------------------------------------------------------------
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

jimport('joomla.form.formfield');

class JFormFieldAssets extends JFormField {

    protected $type = 'Assets';

    protected function getInput() {
        JHTML::_('behavior.framework');
        $document	= &JFactory::getDocument();
        if (!version_compare(JVERSION, '3.0', 'ge')) {
            $checkJqueryLoaded = false;
            $header = $document->getHeadData();
            foreach($header['scripts'] as $scriptName => $scriptData)
            {
                if(substr_count($scriptName,'/jquery')){
                    $checkJqueryLoaded = true;
                }
            }

            //addScript
            if(!$checkJqueryLoaded)
                $document->addScript(JURI::root().$this->element['path'].'js/jquery.min.js');
            $document->addScript(JURI::root().$this->element['path'].'js/yt.js');

            //addStyleSheet
            $document->addStyleSheet(JURI::root().$this->element['path'].'css/yt.css');

        }else{

            //addScript
            $document->addScript(JURI::root().$this->element['path'].'js/yt.js');

            //addStyleSheet
            $document->addStyleSheet(JURI::root().$this->element['path'].'css/yt.css');

        }

        return null;
    }
}
?>