<?php
/*
 * default.php - mod_yt_float_social - YT Float Social by YesTheme
 * ------------------------------------------------------------------------
 *   Author     YesTheme http://yestheme.com
 *   Copyright (C) 2008 - 2013 YesTheme.com. All Rights Reserved.
 *   @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 *   Websites   http://yestheme.com
 * ------------------------------------------------------------------------
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

?>
<div id="yt-float-social<?php echo $module->id; ?>" class="yt-float-social <?php if ($params->get('moduleclass_sfx')) echo ' ' . $params->get('moduleclass_sfx'); ?>">
    <div class="yt-float-social-icon" id="yt-float-social-icon<?php echo $module->id; ?>">
        <img src="<?php echo JURI::base().$float_icon; ?>" alt="Float Social Module for Joomla by YesTheme.com" />
    </div>
    <div class="yt-float-social-main">
    <!-- Facebook: BEGIN -->
<?php if ($enable_facebook): ?>
    <div class="yt-facebook-plugins">
    <div id="fb-root"></div>

    <!-- Facebook Activity Feed: BEGIN -->
    <?php if ($fb_show_activity_feed) : ?>
        <div class="yt-fb-activity-feed">
            <div class="fb-activity"
                 <?php echo $fbActivityFeedParams['dataSite']; ?>
                 <?php echo $fbActivityFeedParams['dataAppID']; ?>
                 <?php echo $fbActivityFeedParams['dataAction']; ?>
                 <?php echo $fbActivityFeedParams['dataWidth']; ?>
                 <?php echo $fbActivityFeedParams['dataHeight']; ?>
                 data-header="<?php echo $fb_activity_feed_show_header; ?>"
                 <?php echo $fbActivityFeedParams['dataColorScheme']; ?>
                 <?php echo $fbActivityFeedParams['dataLinkTarget']; ?>
                 <?php echo $fbActivityFeedParams['dataFont']; ?>
                 data-recommendations="<?php echo $fb_activity_feed_show_recommendations; ?>">
            </div>
        </div>
    <?php endif; ?>
    <!-- Facebook Activity Feed: END -->

    <!-- Facebook Comments Box: BEGIN -->
    <?php if ($fb_show_comments_box) : ?>
        <div class="yt-comments-box">
            <div class="fb-comments"
                 <?php echo $fbCommentsBoxParams['dataHref']; ?>
                 <?php echo $fbCommentsBoxParams['dataWidth']; ?>
                 <?php echo $fbCommentsBoxParams['dataNumPosts']; ?>
                 <?php echo $fbCommentsBoxParams['dataColorScheme']; ?>>
            </div>
        </div>
    <?php endif; ?>
    <!-- Facebook Comments Box: END -->

    <!-- Facebook Like Box: BEGIN -->
    <?php if ($fb_show_like_box) : ?>
        <div class="yt-fb-like-box">
            <div class="fb-like-box"
                 data-href="https://www.facebook.com/<?php echo $fb_like_box_page; ?>"
                 <?php echo $fbLikeBoxParams['dataWidth']; ?>
                 <?php echo $fbLikeBoxParams['dataHeight']; ?>
                 data-show-faces="<?php echo $fb_like_box_show_faces; ?>"
                 <?php echo $fbLikeBoxParams['dataColorScheme']; ?>
                 data-stream="<?php echo $fb_like_box_show_stream; ?>"
                 data-show-border="<?php echo $fb_like_box_show_border; ?>"
                 data-header="<?php echo $fb_like_box_show_header; ?>">
            </div>
        </div>
    <?php endif; ?>
    <!-- Facebook Like Box: END -->
        <div class="fb-copyright">Facebook Plugins <a href="http://yestheme.com" title="Joomla Extension" target="_blank">Joomla Extension</a> by YesTheme.</div>
    </div>
<?php endif; ?>
    <!-- Facebook: END -->

    <!-- Twitter: BEGIN -->
<?php if ($enable_twitter): ?>
    <div class="yt-twitter-widgets">
    <!-- Twitter User Timeline: BEGIN -->
    <?php if ($tw_show_user_timeline) : ?>
        <div class="yt-tw-user-timeline">
            <a class="twitter-timeline"
               <?php if ($twUserTimeLineParams['tw_user_timeline_hash_tag']==""): ?>
               href="https://twitter.com/<?php echo $tw_user_timeline_username.$twUserTimeLineParams['tw_user_timeline_favorites'].$twUserTimeLineParams['tw_user_timeline_list']; ?>"
               <?php else: ?>
               href="https://twitter.com/search/?q=#<?php echo $twUserTimeLineParams['tw_user_timeline_hash_tag']; ?>"
               <?php endif; ?>
               data-widget-id="<?php echo $tw_user_timeline_widget_id; ?>"
               <?php echo $twUserTimeLineParams['tw_user_timeline_theme']; ?>
               <?php echo $twUserTimeLineParams['tw_user_timeline_link_color']; ?>
               <?php echo $twUserTimeLineParams['tw_user_timeline_width']; ?>
               <?php echo $twUserTimeLineParams['tw_user_timeline_height']; ?>
               <?php echo $twUserTimeLineParams['tw_user_timeline_border_color']; ?>
               <?php echo $twUserTimeLineParams['tw_user_timeline_tweet_limit']; ?>
               target="_blank"
               rel="nofollow">Tweets by @<?php echo $tw_user_timeline_username; ?></a>
        </div>
    <?php endif; ?>
    <!-- Twitter User Timeline: END -->
        <div class="tw-copyright">Twitter Widgets <a href="http://yestheme.com" title="Joomla Extension" target="_blank">Joomla Extension</a> by YesTheme.</div>
    </div>
<?php endif; ?>
    <!-- Twitter: END -->

    <!-- Pinterest: BEGIN -->
<?php if ($enable_pinterest) : ?>
    <div class="yt-pinterest-widgets">
    <!-- Pinterest Pin Widget: BEGIN -->
    <?php if ($pin_show_pin_widget) : ?>
        <div class="yt-pin-widget">
        <?php if ($pin_pin_widget_pin_id>0) : ?>
            <a data-pin-do="embedPin"
               href="http://pinterest.com/pin/<?php echo $pin_pin_widget_pin_id; ?>/"
               target="_blank"
               rel="nofollow">
            </a>
        <?php else : ?>
            <div class="yt-error-msg"><?php echo JTEXT::_('YT_FLOAT_SOCIAL_PINTEREST_INVALID_PIN_ID'); ?></div>
        <?php endif; ?>
        </div>
    <?php endif; ?>
    <!-- Pinterest Pin Widget: END -->

    <!-- Pinterest Profile Widget: BEGIN -->
    <?php if ($pin_show_profile_widget) : ?>
        <div class="yt-profile-widget">
        <?php if ($pin_profile_widget_username!="") : ?>
            <a data-pin-do="embedUser"
               href="http://pinterest.com/<?php echo $pin_profile_widget_username; ?>/"
               <?php echo $pinProfileWidgetParams['pin_profile_widget_image_width']; ?>
               <?php echo $pinProfileWidgetParams['pin_profile_widget_board_height']; ?>
               <?php echo $pinProfileWidgetParams['pin_profile_widget_board_width']; ?>
               target="_blank"
               rel="nofollow">
            </a>
        <?php else: ?>
            <div class="yt-error-msg"><?php echo JTEXT::_('YT_FLOAT_SOCIAL_PINTEREST_INVALID_USERNAME'); ?></div>
        <?php endif; ?>
        </div>
    <?php endif; ?>
    <!-- Pinterest Profile Widget: END -->

    <!-- Pinterest Board Widget: BEGIN -->
    <?php if ($pin_show_board_widget) : ?>
        <div class="yt-board-widget">
            <?php if ($pin_board_widget_username!="" && $pin_board_widget_board_short_url!="") : ?>
                <a data-pin-do="embedBoard"
                   href="http://pinterest.com/<?php echo $pin_board_widget_username; ?>/<?php echo $pin_board_widget_board_short_url; ?>"
                    <?php echo $pinBoardWidgetParams['pin_board_widget_image_width']; ?>
                    <?php echo $pinBoardWidgetParams['pin_board_widget_board_height']; ?>
                    <?php echo $pinBoardWidgetParams['pin_board_widget_board_width']; ?>
                   target="_blank"
                   rel="nofollow">
                </a>
            <?php else: ?>
                <div class="yt-error-msg"><?php echo JTEXT::_('YT_FLOAT_SOCIAL_PINTEREST_INVALID_USERNAME_OR_BOARD_URL'); ?></div>
            <?php endif; ?>
        </div>
    <?php endif; ?>
    <!-- Pinterest Board Widget: END -->
        <div class="pin-copyright">Pinterest Widgets <a href="http://yestheme.com" title="Joomla Extension" target="_blank">Joomla Extension</a> by YesTheme.</div>
    </div>
<?php endif; ?>
    <!-- Pinterest: END -->
    </div>
</div>