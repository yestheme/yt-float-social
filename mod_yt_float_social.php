<?php
/*
 * mod_yt_float_social.php - mod_yt_float_social - YT Float Social by YesTheme
 * ------------------------------------------------------------------------
 *   Author     YesTheme http://yestheme.com
 *   Copyright (C) 2008 - 2013 YesTheme.com. All Rights Reserved.
 *   @license   http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
 *   Websites   http://yestheme.com
 * ------------------------------------------------------------------------
 */

// no direct access
defined('_JEXEC') or die('Restricted access');

if (!defined('DS')) {
    define('DS', DIRECTORY_SEPARATOR);
}

require_once(dirname(__FILE__) . DS . 'helpers' . DS . 'helper.php');

JHTML::_('behavior.framework');
$document = JFactory::getDocument();

// Params
$moduleclass_sfx = $params->get('moduleclass_sfx', '');
$enable_facebook = $params->get('enable_facebook', '1');
$enable_twitter = $params->get('enable_twitter', '1');
$enable_pinterest = $params->get('enable_pinterest', '1');
$enable_float = $params->get('enable_float', '1');

$document->addStylesheet(JURI::base(true) . DS . 'modules' . DS . 'mod_yt_float_social' . DS . 'assets' . DS . 'css' . DS . 'mod_yt_float_social.css');
if ($enable_float) {
    $float_icon = $params->get('float_icon', 'modules'.DS.'mod_yt_float_social'.DS.'assets'.DS.'img'.DS.'social.png');
    $float_window_width = $params->get('float_window_width', 300);
    $float_window_height = $params->get('float_window_height', 200);
    $float_window_position = $params->get('float_window_position', 'right');
    $float_window_offset_from = $params->get('float_window_offset_from', 'top');
    $float_window_offset_value = $params->get('float_window_offset_value', 200);
    $float_window_background_color = $params->get('float_window_background_color', '#ffffff');
    $imgSize = getimagesize(JPATH_BASE.DS.$float_icon);
    if (!version_compare(JVERSION, '3.0', 'ge')) {
        $checkJqueryLoaded = false;
        $checkJqueryUILoaded = false;
        $header = $document->getHeadData();
        foreach($header['scripts'] as $scriptName => $scriptData)
        {
            if(substr_count($scriptName,'/jquery')){
                $checkJqueryLoaded = true;
            }
            if(substr_count($scriptName,'/jquery-ui')){
                $checkJqueryUILoaded = true;
            }
        }
        //addScript
        if(!$checkJqueryLoaded) {
            $document->addScript(JURI::base(true) . DS . 'modules' . DS . 'mod_yt_float_social' . DS . 'assets' . DS . 'js' . DS . 'jquery.min.js');
        }
        if(!$checkJqueryUILoaded) {
            $document->addScript(JURI::base(true) . DS . 'modules' . DS . 'mod_yt_float_social' . DS . 'assets' . DS . 'js' . DS . 'jquery-ui-1.10.3.custom.js');
        }
        $document->addScript(JURI::base(true) . DS . 'modules' . DS . 'mod_yt_float_social' . DS . 'assets' . DS . 'js' . DS . 'mod_yt_float_social.js');
    }else{
        //addScript
        $document->addScript(JURI::base(true) . DS . 'modules' . DS . 'mod_yt_float_social' . DS . 'assets' . DS . 'js' . DS . 'jquery-ui-1.10.3.custom.js');
        $document->addScript(JURI::base(true) . DS . 'modules' . DS . 'mod_yt_float_social' . DS . 'assets' . DS . 'js' . DS . 'mod_yt_float_social.js');
    }

    if ($float_window_position=="right" || $float_window_position=="left") {
        $floatJS = "
        jQuery(document).ready(function(){
            jQuery(\"#yt-float-social".$module->id."\").hover(function(){
                    jQuery(\"#yt-float-social".$module->id."\").stop(true, false).animate({{$float_window_position}: \"0\" }, 500, 'easeOutQuint' );
                },
                function(){
                    jQuery(\"#yt-float-social".$module->id."\").stop(true, false).animate({{$float_window_position}: \"-{$float_window_width}\" }, 500, 'easeInQuint' );
                },1000);
        });";
    } else {
        $floatJS = "
        jQuery(document).ready(function(){
            jQuery(\"#yt-float-social".$module->id."\").hover(function(){
                    jQuery(\"#yt-float-social".$module->id."\").stop(true, false).animate({{$float_window_position}: \"0\" }, 500, 'easeOutQuint' );
                },
                function(){
                    jQuery(\"#yt-float-social".$module->id."\").stop(true, false).animate({{$float_window_position}: \"-{$float_window_height}\" }, 500, 'easeInQuint' );
                },1000);
        });";
    }
    $document->addScriptDeclaration($floatJS);

    $floatCSS = "";
    $floatCSS .= "
        #yt-float-social{$module->id} {
            position: fixed;
            text-align: center;
            background-color: {$float_window_background_color};
        }
        #yt-float-social{$module->id} .yt-float-social-main {
            width: {$float_window_width}px;
            height: {$float_window_height}px;
            overflow: hidden;
        }
    ";
    if ($float_window_position=="right" || $float_window_position=="left") {
        $floatCSS .= "
        #yt-float-social{$module->id} {
            position: fixed;
            {$float_window_position}: -{$float_window_width}px;
        }
        ";
    } elseif ($float_window_position=="top" || $float_window_position=="bottom") {
        $floatCSS .= "
        #yt-float-social{$module->id} {
            position: fixed;
            {$float_window_position}: -{$float_window_height}px;
        }
        ";
    }
    if ($float_window_offset_from=="bottom") {
        $floatCSS .= "
            #yt-float-social{$module->id} {
            {$float_window_offset_from}: {$float_window_offset_value}px;
            }
            #yt-float-social-icon{$module->id} {
                bottom: 0px;
            }
        ";
    } elseif ($float_window_offset_from=="right") {
        $floatCSS .= "
            #yt-float-social{$module->id} {
            {$float_window_offset_from}: {$float_window_offset_value}px;
            }
            #yt-float-social-icon{$module->id} {
                right: 0px;
            }
        ";
    } else {
        $floatCSS .= "
            #yt-float-social{$module->id} {
            {$float_window_offset_from}: {$float_window_offset_value}px;
            }
        ";
    }
    if (is_array($imgSize)) {
        $floatCSS .= "
        #yt-float-social-icon{$module->id} {
            width : {$imgSize[0]}px;
            height : {$imgSize[1]}px;
            position: absolute;
        }
        ";
        if ($float_window_position=="top") {
            $floatCSS .= "
            #yt-float-social-icon{$module->id} {
                bottom: -{$imgSize[1]}px;
            }
            ";
        } elseif ($float_window_position=="bottom") {
            $floatCSS .= "
            #yt-float-social-icon{$module->id} {
                top: -{$imgSize[1]}px;
            }
            ";
        } elseif ($float_window_position=="right") {
            $floatCSS .= "
            #yt-float-social-icon{$module->id} {
                left: -{$imgSize[0]}px;
            }
            ";
        } elseif ($float_window_position=="left") {
            $floatCSS .= "
            #yt-float-social-icon{$module->id} {
                right: -{$imgSize[0]}px;
            }
            ";
        }
    }
    $document->addStyleDeclaration($floatCSS);
}


// Facebook
if ($enable_facebook) {
    $fb_show_activity_feed = $params->get('fb_show_activity_feed', '1');
    $fb_show_comments_box = $params->get('fb_show_comments_box', '1');
    $fb_show_like_box = $params->get('fb_show_like_box', '1');

    // get Facebook Activity Feed Configuration
    if ($fb_show_activity_feed) {
        $fb_activity_feed_domain = $params->get('fb_activity_feed_domain', '');
        $fb_activity_feed_app_id = $params->get('fb_activity_feed_app_id', '');
        $fb_activity_feed_action = $params->get('fb_activity_feed_action', '');
        $fb_activity_feed_width = intval($params->get('fb_activity_feed_width', ''));
        $fb_activity_feed_height = intval($params->get('fb_activity_feed_height', ''));
        $fb_activity_feed_show_header = $params->get('fb_activity_feed_show_header', '1');
        $fb_activity_feed_color_scheme = $params->get('fb_activity_feed_color_scheme', 'light');
        $fb_activity_feed_link_target = $params->get('fb_activity_feed_link_target', '_blank');
        $fb_activity_feed_font = $params->get('fb_activity_feed_font', '');
        $fb_activity_feed_show_recommendations = $params->get('fb_activity_feed_show_recommendations', '');
        $fb_activity_feed_show_recommendations = (is_array($fb_activity_feed_show_recommendations)) ? $fb_activity_feed_show_recommendations[0] : "false";

        $fbActivityFeedParams = array();
        $fbActivityFeedParams['dataSite'] = ($fb_activity_feed_domain!="") ? " data-site=\"".$fb_activity_feed_domain . "\"" : "";
        $fbActivityFeedParams['dataAppID'] = ($fb_activity_feed_app_id!="") ? " data-app-id=\"".$fb_activity_feed_app_id . "\"" : "";
        $fbActivityFeedParams['dataAction'] = ($fb_activity_feed_action!="") ? " data-action=\"".$fb_activity_feed_action . "\"" : "";
        $fbActivityFeedParams['dataWidth'] = ($fb_activity_feed_width>0) ? " data-width=\"".$fb_activity_feed_width . "\"" : "";
        $fbActivityFeedParams['dataHeight'] = ($fb_activity_feed_height>0) ? " data-height=\"".$fb_activity_feed_height . "\"" : "";
        $fbActivityFeedParams['dataColorScheme'] = ($fb_activity_feed_color_scheme=="dark") ? " data-colorscheme=\"".$fb_activity_feed_color_scheme . "\"" : "";
        $fbActivityFeedParams['dataLinkTarget'] = ($fb_activity_feed_link_target=="_top" || $fb_activity_feed_link_target=="_parent") ? " data-linktarget=\"".$fb_activity_feed_link_target . "\"" : "";
        $fbActivityFeedParams['dataFont'] = ($fb_activity_feed_font!="") ? " data-font=\"".$fb_activity_feed_font . "\"" : "";
    }

    // get Facebook Comments Box Configuration
    if ($fb_show_comments_box) {
        $fb_comments_box_url = $params->get('fb_comments_box_url', '');
        $fb_comments_box_width = intval($params->get('fb_comments_box_width', ''));
        $fb_comments_box_num_posts = $params->get('fb_comments_box_num_posts', '');
        $fb_comments_box_color_scheme = $params->get('fb_comments_box_color_scheme', 'light');

        $fbCommentsBoxParams = array();
        $fbCommentsBoxParams['dataHref'] = ($fb_comments_box_url!="") ? " data-href=\"".$fb_comments_box_url . "\"" : " data-href=\"".JURI::current()."\"";
        $fbCommentsBoxParams['dataWidth'] = ($fb_comments_box_width>0) ? " data-width=\"".$fb_comments_box_width . "\"" : "";
        $fbCommentsBoxParams['dataNumPosts'] = ($fb_comments_box_num_posts!="") ? " data-num-posts=\"".$fb_comments_box_num_posts . "\"" : "";
        $fbCommentsBoxParams['dataColorScheme'] = ($fb_comments_box_color_scheme=="dark") ? " data-colorscheme=\"".$fb_comments_box_color_scheme . "\"" : "";
    }

    // get Facebook Like Box Configuration
    if ($fb_show_like_box) {
        $fb_like_box_page = $params->get('fb_like_box_page', '516045945131937');
        $fb_like_box_width = intval($params->get('fb_like_box_width', '0'));
        $fb_like_box_height = intval($params->get('fb_like_box_height', '0'));
        $fb_like_box_show_faces = $params->get('fb_like_box_show_faces', '1');
        $fb_like_box_color_scheme = $params->get('fb_like_box_color_scheme', 'light');
        $fb_like_box_show_stream = $params->get('fb_like_box_show_stream', '1');
        $fb_like_box_show_border = $params->get('fb_like_box_show_border', '1');
        $fb_like_box_show_header = $params->get('fb_like_box_show_header', '1');

        $fbLikeBoxParams = array();
        $fbLikeBoxParams['dataWidth'] = "";
        $fbLikeBoxParams['dataHeight'] = "";
        if ($fb_like_box_width>0) {
            $fbLikeBoxParams['dataWidth'] = " data-width=\"{$fb_like_box_width}\"";
        }
        if ($fb_like_box_height>0) {
            $fbLikeBoxParams['dataHeight'] = " data-height=\"{$fb_like_box_height}\"";
        }
        $fbLikeBoxParams['dataColorScheme'] = ($fb_like_box_color_scheme=="dark") ? " data-colorscheme=\"{$fb_like_box_color_scheme}\"" : "";

    }

    $fb_script = "(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = \"//connect.facebook.net/en_US/all.js#xfbml=1\";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));";
    $document->addScriptDeclaration($fb_script);
}

// Twitter
if ($enable_twitter) {
    $tw_show_user_timeline = $params->get('tw_show_user_timeline', '1');

    //get Twitter User Timeline Widget Configuration
    if ($tw_show_user_timeline) {
        $tw_user_timeline_username = $params->get('tw_user_timeline_username', '');
        $tw_user_timeline_widget_id = $params->get('tw_user_timeline_widget_id', '');
        $tw_user_timeline_favorites = intval($params->get('tw_user_timeline_favorites', '0'));
        $tw_user_timeline_list = $params->get('tw_user_timeline_list', '');
        $tw_user_timeline_hash_tag = $params->get('tw_user_timeline_hash_tag', '');
        $tw_user_timeline_theme = $params->get('tw_user_timeline_theme', '');
        $tw_user_timeline_link_color = $params->get('tw_user_timeline_link_color', '');
        $tw_user_timeline_width = intval($params->get('tw_user_timeline_width', ''));
        $tw_user_timeline_height = intval($params->get('tw_user_timeline_height', ''));
        $tw_user_timeline_border_color = $params->get('tw_user_timeline_border_color', '');
        $tw_user_timeline_tweet_limit = intval($params->get('tw_user_timeline_tweet_limit', ''));
        if ($tw_user_timeline_tweet_limit>20) {
            $tw_user_timeline_tweet_limit = 20;
        }
        $twUserTimeLineParams = array();
        $twUserTimeLineParams['tw_user_timeline_favorites'] = ($tw_user_timeline_favorites==1) ? "/favorites" : "";
        $twUserTimeLineParams['tw_user_timeline_list'] = ($tw_user_timeline_list!="" && $tw_user_timeline_favorites!=1) ? "/{$tw_user_timeline_list}" : "";
        $twUserTimeLineParams['tw_user_timeline_hash_tag'] = ($tw_user_timeline_theme=="dark") ? " data-theme=\"".$tw_user_timeline_theme . "\"" : "";
        $twUserTimeLineParams['tw_user_timeline_theme'] = ($tw_user_timeline_theme=="dark") ? " data-theme=\"".$tw_user_timeline_theme . "\"" : "";
        $twUserTimeLineParams['tw_user_timeline_link_color'] = ($tw_user_timeline_link_color!="") ? " data-link-color=\"".$tw_user_timeline_link_color . "\"" : "";
        $twUserTimeLineParams['tw_user_timeline_width'] = ($tw_user_timeline_width>=180 && $tw_user_timeline_width<=520) ? " width=\"{$tw_user_timeline_width}\"" : "";
        $twUserTimeLineParams['tw_user_timeline_height'] = ($tw_user_timeline_height>=200) ? " height=\"{$tw_user_timeline_height}\"" : "";
        $twUserTimeLineParams['tw_user_timeline_border_color'] = ($tw_user_timeline_border_color!="") ? " data-border-color=\"".$tw_user_timeline_border_color . "\"" : "";
        $twUserTimeLineParams['tw_user_timeline_tweet_limit'] = ($tw_user_timeline_tweet_limit>0 && $tw_user_timeline_tweet_limit<=20) ? " data-tweet-limit=\"".$tw_user_timeline_tweet_limit . "\"" : "";
    }

    $tw_script = "!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=\"//platform.twitter.com/widgets.js\";fjs.parentNode.insertBefore(js,fjs);}}(document,\"script\",\"twitter-wjs\");";
    $document->addScriptDeclaration($tw_script);
}

//Pinterest
if ($enable_pinterest) {
    $pin_show_pin_widget = $params->get('pin_show_pin_widget', "1");
    $pin_show_profile_widget = $params->get('pin_show_profile_widget', "1");
    $pin_show_board_widget = $params->get('pin_show_board_widget', "1");

    //get Pinterest Pin Widget Configuration
    if ($pin_show_pin_widget) {
        $pin_pin_widget_pin_id = $params->get('pin_pin_widget_pin_id', '');
    }

    //get Pinterest Profile Widget Configuration
    if ($pin_show_profile_widget) {
        $pin_profile_widget_username = $params->get('pin_profile_widget_username', '');
        $pin_profile_widget_image_width = $params->get('pin_profile_widget_image_width', '');
        $pin_profile_widget_board_height = $params->get('pin_profile_widget_board_height', '');
        $pin_profile_widget_board_width = $params->get('pin_profile_widget_board_width', '');
        if ($pin_profile_widget_username!="") {
            $pinProfileWidgetParams = array();
            $pinProfileWidgetParams['pin_profile_widget_image_width'] = ($pin_profile_widget_image_width<60) ? "" : "data-pin-scale-width=\"{$pin_profile_widget_image_width}\"";
            $pinProfileWidgetParams['pin_profile_widget_board_height'] = ($pin_profile_widget_board_height<60) ? "" : "data-pin-board-height=\"{$pin_profile_widget_board_height}\"";
            $pinProfileWidgetParams['pin_profile_widget_board_width'] = ($pin_profile_widget_board_width<60) ? "" : "data-pin-board-width=\"{$pin_profile_widget_board_width}\"";
        }
    }

    //get Pinterest Board Widget Configuration
    if ($pin_show_board_widget) {
        $pin_board_widget_username = $params->get('pin_board_widget_username', '');
        $pin_board_widget_board_short_url = $params->get('pin_board_widget_board_short_url', '');
        $pin_board_widget_image_width = $params->get('pin_board_widget_image_width', '');
        $pin_board_widget_board_height = $params->get('pin_board_widget_board_height', '');
        $pin_board_widget_board_width = $params->get('pin_board_widget_board_width', '');
        if ($pin_board_widget_username!="" && $pin_board_widget_board_short_url!="") {
            $pinBoardWidgetParams = array();
            $pinBoardWidgetParams['pin_board_widget_image_width'] = ($pin_board_widget_image_width<60) ? "" : "data-pin-scale-width=\"{$pin_board_widget_image_width}\"";
            $pinBoardWidgetParams['pin_board_widget_board_height'] = ($pin_board_widget_board_height<60) ? "" : "data-pin-board-height=\"{$pin_board_widget_board_height}\"";
            $pinBoardWidgetParams['pin_board_widget_board_width'] = ($pin_board_widget_board_width<60) ? "" : "data-pin-board-width=\"{$pin_board_widget_board_width}\"";
        }
    }

    $pin_script = "(function(d){
  var f = d.getElementsByTagName('SCRIPT')[0], p = d.createElement('SCRIPT');
  p.type = 'text/javascript';
  p.async = true;
  p.src = '//assets.pinterest.com/js/pinit.js';
  f.parentNode.insertBefore(p, f);
}(document));";
    $document->addScriptDeclaration($pin_script);
}

require (JModuleHelper::getLayoutPath('mod_yt_float_social', 'default'));